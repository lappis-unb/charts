# LAPPIS Helm Chart
[![pipeline status](https://gitlab.com/lappis-unb/charts/badges/master/pipeline.svg)](https://gitlab.com/lappis-unb/charts/commits/master)

To use this Chart repository, run

```bash
helm repo add lappis https://charts.lappis.rocks
```

### lappisudo Example 

```
helm install lappis/lappisudo
```